package at.srsyntax.deathcountertab;

import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.io.IOException;

/**
 * CONFIDENTIAL
 * Unpublished Copyright (c) 19.12.2020 Marcel Haberl, All Rights Reserved.
 * -
 * NOTICE:  All information contained herein is, and remains the property of Marcel Haberl. The intellectual and technical concepts contained
 * herein are proprietary to Marcel Haberl and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 * from Marcel Haberl.  Access to the source code contained herein is hereby forbidden to anyone without written permission
 * Confidentiality and Non-disclosure agreements explicitly covering such access.
 * -
 * The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 * information that is confidential and/or proprietary, and is a trade secret, of Marcel Haberl.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 * OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF Marcel Haberl IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 * LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 * TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
 */
public class DeathCounterTab extends JavaPlugin implements Listener {

    private Scoreboard scoreboard;
    private String suffix;

    @Override
    public void onEnable() {
        scoreboard = getServer().getScoreboardManager().getNewScoreboard();
        getServer().getPluginManager().registerEvents(this, this);

        if (!getDataFolder().exists())
            getDataFolder().mkdirs();

        final File file = new File(getDataFolder().getPath(), "config.yml");
        final YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
        if (!file.exists()) {
            try {
                file.createNewFile();

                configuration.set("suffix", "&e %DEATHS%");

                configuration.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        suffix = configuration.getString("suffix");
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        scoreboard.registerNewTeam(player.getName());
        scoreboard.getTeam(player.getName()).addEntry(player.getName());
        updatePlayer(player);
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        scoreboard.getTeam(event.getPlayer().getName()).unregister();
    }

    @EventHandler
    public void onPlayerRespawnEvent(PlayerRespawnEvent event) {
        updatePlayer(event.getPlayer());
    }

    public void updatePlayer(Player player) {
        scoreboard.getTeam(player.getName()).setSuffix(suffix.replace("&", "§").replace("%DEATHS%", String.valueOf(player.getStatistic(Statistic.DEATHS))));
        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> onlinePlayer.setScoreboard(scoreboard));
    }
}
